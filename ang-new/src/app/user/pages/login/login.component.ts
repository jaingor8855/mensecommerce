import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/service/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   LoginForm : FormGroup;
   CheckForm = false;
   errorMasg = "";
  constructor(
    private _fb : FormBuilder,
    private _logiserv : LoginService,
    private _router : Router
  ) { 
    this.LoginForm = this._fb.group({
      email : ["", [Validators.required, Validators.email]],
      password : ["", [Validators.required]]
    })
  }

  ngOnInit(): void {
  }
  login(){
    if(this.LoginForm.invalid){
      this.CheckForm = true;
      return;
    }
    this._logiserv.do_login(this.LoginForm.value).subscribe((result)=>{
      localStorage.setItem("jwt_token", result.token);
      this._router.navigate(["/"])
    },(err)=>{
      if(err.error.type==1)
      {
        this.errorMasg="This Email Id is not registered!";
      }
      if(err.error.type==2)
      {
        this.errorMasg="This password is incorrect !";
      }
    })
  }

}
