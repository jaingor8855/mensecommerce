import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppointmentService } from 'src/app/service/appointment.service';



@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css']
})
export class AppointmentComponent implements OnInit {
  appointmentForm : FormGroup;
  CheckForm=false;
  constructor(
    private _fb : FormBuilder,
    private _router :Router,
    private _appointmentserv : AppointmentService
  ) { 
    this.appointmentForm = this._fb.group({
        selected_a_service :  ["", Validators.required],
          select_doctor : ["", Validators.required],
          your_name : ["", Validators.required],
          your_email : ["", Validators.required],
          appointment_date :["", Validators.required],
          appointment_time : ["", Validators.required],

    })
  }

  ngOnInit(): void {
  }
  submit(){
    if(this.appointmentForm.invalid){
      this.CheckForm=true;
      return
    }
    this._appointmentserv.add(this.appointmentForm.value).subscribe((result)=>{
      this._router.navigate(["/"])
    })
  }

}
