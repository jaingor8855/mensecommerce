import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../../service/login.service'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
   signinForm: FormGroup;
   checkForm = false;
  constructor(
    private _fb : FormBuilder,
    private _loginserv :LoginService,
    private _router : Router
  ) {
    this.signinForm = this._fb.group({
      full_name : ["", Validators.required],
      email : ["",[Validators.required, Validators.email]],
      password : ["", Validators.required],
      re_password : ["", Validators.required],
      address : ["", Validators.required],
      city : ["", Validators.required],
      contact : ["", Validators.required]

    })
   }

  ngOnInit(): void {
  }

  signin(){
    if(this.signinForm.invalid){
      this.checkForm=true;
      return;
    }
    this._loginserv.signin(this.signinForm.value).subscribe((result)=>{
      this._router.navigate(["/"])
    })
  }
}
