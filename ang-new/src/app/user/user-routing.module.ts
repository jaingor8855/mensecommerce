import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { AppointmentComponent } from './pages/appointment/appointment.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { PricingComponent } from './pages/pricing/pricing.component';
import { ServiceComponent } from './pages/service/service.component';
import { SignupComponent } from './pages/signup/signup.component';
import { TeamComponent } from './pages/team/team.component';
import { TestimonialComponent } from './pages/testimonial/testimonial.component';
import { UserComponent } from './user.component';

const routes: Routes = [
  {
    path : "",
    component : UserComponent,
    children : [
      {
        path : "",
        component : HomeComponent
      },
      {
        path : "about",
        component : AboutComponent
      },
      {
        path : "service",
        component : ServiceComponent
      },
      {
        path : "pricing",
        component : PricingComponent
      },
      {
        path : "team",
        component : TeamComponent
      },
      {
        path : "testimonial",
        component : TestimonialComponent
      },
      {
        path : "appointment",
        component : AppointmentComponent
      },
      {
        path : "contact",
        component : ContactComponent
      },
      {
        path : "signup",
        component : SignupComponent
      },
      {
        path : "login",
        component : LoginComponent
      },
      {
        path : "my-account",
        component : MyAccountComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
