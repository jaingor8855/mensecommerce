import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../service/login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
adminLoginForm : FormGroup;
checkForm=false;
errorMsg=""
  constructor(
    private _fb : FormBuilder,
    private _loginser : LoginService,
    private _router : Router  
  ) {
    this.adminLoginForm = this._fb.group({
      username :["", Validators.required],
      password :["", Validators.required]
    })
   }

  ngOnInit(): void {
  }
  submit(){
    if(this.adminLoginForm.invalid){
      this.checkForm=true;
      return
    }
    this._loginser.do_login(this.adminLoginForm.value).subscribe((result)=>{
      localStorage.setItem("jwt_token", result.token);
      this._router.navigate(["/admin/dash"])
    },err=>{
      if(err.error.type ==1)
      {
        this.errorMsg="This Username and Password is Incorrect!"
      }
      if(err.error.type==2)
      {
        this.errorMsg="This password is Incorrect"
      }
    })
  }

}
