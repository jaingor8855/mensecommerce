import { Component, OnInit } from '@angular/core';
import { AppointmentService } from 'src/app/service/appointment.service';
import { ViewChild } from '@angular/core';


@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css']
})
export class AppointmentComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn:any
  allAppointment:any[]=[]
  appointment:any
  constructor(
    private _apponitmentserv : AppointmentService
  ) {
    this._apponitmentserv.getAll().subscribe((result)=>{
      this.allAppointment = result
    })
   }

  ngOnInit(): void {
  }
  askDelete(obj:any){
    this.appointment=obj;
  }
  confDelete(){
    this._apponitmentserv.delete(this.appointment).subscribe((result)=>{
      let n = this.allAppointment.indexOf(this.appointment);
      this.allAppointment.splice(n, 1);
      this.closeBtn.nativeElement.click();
    })
  }

}
