import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from '../admin/admin.component'
import { AppointmentComponent } from '../admin/pages/appointment/appointment.component';
import { DashComponent } from './pages/dash/dash.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {
    path : "",
    component : AdminComponent,
    children : [
      
      {
        path : "",
        component : LoginComponent
      },
      {
        path : "dash",
        component : DashComponent
      },
      {
        path : "appointment",
        component : AppointmentComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
