import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  apiurlSignin = environment.API_URL+"/api/user/signin";
  apiurlAuth = environment.API_URL+"/api/user/auth";

  constructor(
    private _http : HttpClient
  ) { }
  signin( obj:any){
     return this._http.post<any>(this.apiurlSignin,obj)
  }
  do_login(obj:any){
    return this._http.post<any>(this.apiurlAuth,obj)
  }
}
